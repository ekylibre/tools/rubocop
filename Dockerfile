FROM ruby:2.6-alpine

RUN gem install rubocop --version=1.11.0

COPY .rubocop.yml /
